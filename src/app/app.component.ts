import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'app works!';
    
	tabs = [
        {
        	status : true,
            title : 'Tab 1',
            tabData : 'Dynamic content for tab1'
        },
        {
        	status : false,
            title : 'Tab 2',
            tabData : 'Dynamic content for tab2'
        },
        {
        	status : false,
            title : 'Tab 3',
            tabData : 'Dynamic content for tab3'
        },
        {
        	status : false,
            title : 'Tab 4',
            tabData : 'Dynamic content for tab4'
        }
    ]
}
